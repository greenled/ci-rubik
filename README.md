# CI Rubik

Python script to generate a build matrix in GitLab CI. See GitLab [Dynamic Child Pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html#dynamic-child-pipelines) and [trigger keyword](https://docs.gitlab.com/ee/ci/yaml/README.html#trigger-child-pipeline-with-generated-configuration-file) documentation for background.

### Usage

1. Define two stages: one for generating build matrix jobs and another for triggering them

   ```yaml
   # You can reuse your already-defined stages as well, just make sure build
   # matrix jobs generation happens before triggering
   stages:
     - setup
     - triggers
   ```
2. Define a job you want to use as a template for the matrix
   ```yaml
   # Using a hidden key (starting job name with `.`) as in this example is
   # recommended, but not necessary. It will just prevent template job from
   # being run as a regular one.
   .template:
     image: bash
     script:
       # Use as many environment variables as you need 
       - echo $FOO $BAR $BAZ
   ```
3. Define a job to run the script
   ```yaml
   generate-jobs:
     stage: setup
     # Here you are using a Docker image which bundles the script and
     # Python 3.6 so, make sure your runner supports Docker
     image:
       name: registry.gitlab.io/greenled/ci-rubik:latest
       entrypoint: [""]
     script:
       - cat .gitlab-ci.yml | ./generate_jobs > matrix-jobs.yml
     variables:
       # Uncomment if you used a template job name different from ".template"
       # (e.g. .template_job)
       #MATRIX_TEMPLATE_JOB_NAME: .template_job
       
       # Environment variables to be expanded in template job. Separate values
       # with ",". Note in this example the BAZ variable won't be expanded, as
       # it has not been assigned any value here. 
       FOO: "1,2"
       BAR: "3,4"
     artifacts:
       paths:
         - matrix-jobs.yml
   ```
4. Define a job that will run the generated jobs
   ```yaml
   trigger-jobs:
     stage: triggers
     trigger:
       include:
         - artifact: matrix-jobs.yml
           job: generate-jobs
   ```
