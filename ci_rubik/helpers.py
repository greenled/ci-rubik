"""
Several utility functions
"""
import yaml
import os

from itertools import product


def get_job_variables_names(job_definition):
    """
    Extract variables names from a CI job definition
    >>> get_job_variables_names({'variables': {'A': '1,2', 'B': '3,4'}})
    ['A', 'B']
    >>> get_job_variables_names({})
    []
    >>> get_job_variables_names({"variables": {}})
    []

    :param job_definition: A GitLab CI job definition
    """

    if "variables" in job_definition.keys() and job_definition["variables"] is not None:
        return [k for k in job_definition["variables"].keys()]
    else:
        return []


def extract_environment_variables(names, list_value_separator):
    """
    Extract environment variables with given names, splitting their values
    by the given separator
    >>> os.environ = {'A': '1,2', 'B': '3,4', 'C': '5,6'}
    >>> extract_environment_variables(['A', 'B'], ',')
    {'A': ['1', '2'], 'B': ['3', '4']}
    """
    return {k: os.environ[k].split(list_value_separator) for k in names}


def spread(dict_to_spread):
    """
    Spread variables
    >>> spread({'A': ['1', '2'], 'B': ['3', '4']})
    [[('A', '1'), ('A', '2')], [('B', '3'), ('B', '4')]]
    """
    return [[(k, v) for v in dict_to_spread[k]] for k in dict_to_spread.keys()]


def cartesian_product(variables):
    """
    Calculate Cartesian product of a list of lists
    >>> cartesian_product([[('A', '1'), ('A', '2')], [('B', '3'), ('B', '4')]])
    [(('A', '1'), ('B', '3')), (('A', '1'), ('B', '4')), (('A', '2'), ('B', '3')), (('A', '2'), ('B', '4'))]
    """
    return [v for v in product(*variables)]


def join(variables):
    """
    Transform a list of lists of tuples into a list of dicts
    >>> join([[('A', '1'), ('B', '3')], [('A', '1'), ('B', '4')], [('A', '2'), ('B', '3')], [('A', '2'), ('B', '4')]])
    [{'A': '1', 'B': '3'}, {'A': '1', 'B': '4'}, {'A': '2', 'B': '3'}, {'A': '2', 'B': '4'}]
    """
    return [dict(row) for row in variables]


def generate_configuration(template, variables):
    """
    Generate GitLab CI configuration by expanding all combinations of
    given variables in the given template
    >>> template = {
    ... 'image': 'bash',
    ... 'script': ['echo $A $B']
    ... }
    >>> variables = {'A': ['1', '2'], 'B': ['3', '4']}
    >>> generate_configuration(template, variables)
    'matrix-job-1:\\n  image: bash\\n  script:\\n  - echo 1 3\\nmatrix-job-2:\\n  image: bash\\n  script:\\n  - echo 1 4\\nmatrix-job-3:\\n  image: bash\\n  script:\\n  - echo 2 3\\nmatrix-job-4:\\n  image: bash\\n  script:\\n  - echo 2 4\\n'
    """
    # Generate environments
    environments = join(cartesian_product(spread(variables)))
    configuration = ''
    # Backup current OS environment variables
    old_environ = os.environ
    for i, environ in enumerate(environments, start=1):
        # Encapsulate job template in a job definition
        job_definition = {f'matrix-job-{i}': template}
        # Render job definition
        rendered_job_definition = yaml.dump(job_definition)
        # Replace OS variables
        os.environ = environ
        # Expand variables in rendered job definition
        expanded = os.path.expandvars(rendered_job_definition)
        # Print job text to output file
        configuration += expanded
    # Restore saved OS environment variables
    os.environ = old_environ
    return configuration
