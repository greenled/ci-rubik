import yaml
import os
import sys

from ci_rubik.helpers import get_job_variables_names, extract_environment_variables, generate_configuration


def run():
    # Separator for variables' values
    variable_values_separator = os.getenv('MATRIX_VARIABLE_VALUES_SEPARATOR', ',')

    # Name of the CI job to be used as a template
    template_job_name = os.getenv('MATRIX_TEMPLATE_JOB_NAME', '.template')

    ci_job_name = os.getenv('CI_JOB_NAME')

    # Open the main configuration file
    full_document = yaml.safe_load(sys.stdin)

    # Extract current CI job
    ci_job = full_document[ci_job_name]

    # Extract job template
    job_template = full_document[template_job_name]

    # Extract current CI job variables names
    keys = [v for v in get_job_variables_names(ci_job) if v not in [
        'MATRIX_VARIABLE_VALUES_SEPARATOR',
        'MATRIX_TEMPLATE_JOB_NAME'
    ]]

    # Extract current CI job variables with their values split
    variables = extract_environment_variables(keys, variable_values_separator)

    # Generate GitLab CI configuration
    configuration = generate_configuration(job_template, variables)

    return configuration
